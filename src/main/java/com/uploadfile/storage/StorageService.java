package com.uploadfile.storage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.uploadfile.entity.UploadFile;
import com.uploadfile.repository.FileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
public class StorageService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private AmazonS3 amazonS3client;

    @Value("${jsa.s3.bucket}")
    private String bucketName;


	public void uploadFileToStorage(MultipartFile multipartFile, String userName){
		try {
            File file = convertMultiPartToFile(multipartFile);
            String fileName = multipartFile.getOriginalFilename();
            uploadFileTosAmazonS3bucket(fileName, file);
            file.delete();

            UploadFile uploadFile = new UploadFile();
            uploadFile.setFileName(fileName);
            uploadFile.setUserName(userName);
            fileRepository.save(uploadFile);

        } catch (Exception e) {
        	e.printStackTrace();
        }
	}


    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File result = new File(file.getOriginalFilename());
        FileOutputStream fileOutputStream = new FileOutputStream(result);
        fileOutputStream.write(file.getBytes());
        fileOutputStream.close();
        return result;
    }

    private void uploadFileTosAmazonS3bucket(String fileName, File file) {
        amazonS3client.putObject(new PutObjectRequest(bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

}