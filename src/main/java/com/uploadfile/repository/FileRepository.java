package com.uploadfile.repository;


import com.uploadfile.entity.UploadFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<UploadFile, Long>{
}
