package com.uploadfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AmazonS3Upload {
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(AmazonS3Upload.class, args);
    }

}