package com.uploadfile.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.uploadfile.storage.StorageService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class RestUploadController {

	@Autowired
	StorageService storageService;

    @PostMapping("/uploadfile")
    public String uploadFileMulti(@RequestParam("uploadfile") MultipartFile file, @RequestParam("userName") String userName) throws Exception {
    	try {
			storageService.uploadFileToStorage(file, userName);
			return "You successfully uploaded file to Amazon S3 - " + file.getOriginalFilename();
		} catch (AmazonS3Exception e) {
			e.printStackTrace();
			return "Access Denied";
		}
    }
}
