package com.uploadfile.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "upload_file_data")
public class UploadFile implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "file_name")
    private String fileName;


    public void setId(Long id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


}
