# Amazon S3 uploader

Here's description how to run AmazonS3Upload.

### Prerequisites

 * [Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html), version 1.8.
 * [Apache Maven](http://maven.apache.org), version 3.3.9 or higher.
 * [Spring Boot](https://projects.spring.io/spring-boot/), version 1.5.4 or higher.
 * [Amazon S3](http://aws.amazon.com/s3/) storage credentials (bucket name, key and secret) to store uploaded files.
 
### How to run and configurate

* sudo mysql -u [USER] -p [PASSWORD] database < filePath/uploaded_file.sql
* add an application.properties storage credentials (bucket name, key, secret and region)
* Point your browser to http://localhost:8080